﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;

namespace Project
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        XmlDocument xdoc;
        string path;
        private void button1_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "XML|*.xml";
            if (ofd.ShowDialog() == DialogResult.OK)
            {
              xdoc = new XmlDocument();
              path = ofd.FileName;
                //xdoc.Load(@"C:\Users\harry\Desktop\my.xml");
                xdoc.Load(path);
                textBox1.Text = xdoc.SelectSingleNode("people/person/name").InnerText;
                numericUpDown1.Value=Convert.ToInt32(xdoc.SelectSingleNode("people/person/age").InnerText);
                textBox2.Text = xdoc.SelectSingleNode("people/person/email").InnerText;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            xdoc.SelectSingleNode("people/person/name").InnerText=textBox1.Text;
            xdoc.SelectSingleNode("people/person/age").InnerText = numericUpDown1.Value.ToString();
            xdoc.SelectSingleNode("people/person/email").InnerText= textBox2.Text;
            xdoc.Save(path);
            
        }

        private void button3_Click(object sender, EventArgs e)
        {
            XmlTextWriter xwriter = new XmlTextWriter(@"C:\Users\harry\Desktop\Mahesh\xdoc2.xml", Encoding.UTF8);
            xwriter.Formatting = Formatting.Indented;
            xwriter.WriteStartElement("People");
            xwriter.WriteStartElement("Person");
            xwriter.WriteStartElement("Name");
            xwriter.WriteString(textBox1.Text);
            xwriter.WriteEndElement();
            xwriter.WriteStartElement("Age");
            xwriter.WriteString(numericUpDown1.Value.ToString());
            xwriter.WriteEndElement();
            xwriter.WriteStartElement("Email");
            xwriter.WriteString(textBox2.Text);
            xwriter.WriteEndElement();
            xwriter.WriteEndElement();
            xwriter.WriteEndElement();
            xwriter.Close();

        }

        private void button4_Click(object sender, EventArgs e)
        {
            xdoc = new XmlDocument();
            xdoc.Load(@"C:\Users\harry\Desktop\Mahesh\xdoc2.xml");
            XmlNode person = xdoc.CreateElement("Person");
            XmlNode name = xdoc.CreateElement("Name");
            name.InnerText = textBox1.Text;
            person.AppendChild(name);
            XmlNode age = xdoc.CreateElement("Age");
            age.InnerText = numericUpDown1.Value.ToString();
            person.AppendChild(age);
            XmlNode email = xdoc.CreateElement("Email");
            email.InnerText = textBox2.Text;
            person.AppendChild(email);
            xdoc.DocumentElement.AppendChild(person);
            xdoc.Save(@"C:\Users\harry\Desktop\Mahesh\xdoc2.xml");
        }

        private void button5_Click(object sender, EventArgs e)
        {

          
            
                xdoc = new XmlDocument();
                xdoc.Load(@"C:\Users\harry\Desktop\Mahesh\xdoc2.xml");
                foreach (XmlNode xnode in xdoc.SelectNodes("People/Person"))
                    if (xnode.SelectSingleNode("Age").InnerText == textBox3.Text)
                        xnode.ParentNode.RemoveChild(xnode);
                xdoc.Save(@"C:\Users\harry\Desktop\Mahesh\xdoc2.xml");
            
           
        }
    }
}
