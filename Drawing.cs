﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Project
{
    public partial class Drawing : Form
    {
        public Drawing()
        {
            InitializeComponent();
        }

        private void panel1_Click(object sender, EventArgs e)
        {
            //Pen pen = new Pen(Color.Green,50);
            SolidBrush sb = new SolidBrush(Color.Purple);
            Graphics g = panel1.CreateGraphics();
            //Point[] points = { new Point(0, 150), new Point(0, 0), new Point(150, 0) };
            //g.DrawRectangle(pen, 50, 50, 100, 100);
            //g.DrawEllipse(pen,75, 20, 50, 50);
            //g.DrawEllipse(pen,75, 125, 50, 50);
            //g.DrawEllipse(pen, 25, 75, 50, 50);
            //g.DrawEllipse(pen, 125, 75, 50, 50);
            //g.DrawEllipse(pen, 75, 75, 50, 50);
            //g.DrawPolygon(pen, points);
            //g.DrawArc(pen, 20, 20, 80, 80,0,360);
            //g.DrawBezier(pen, new Point(20, 20), new Point(30, 60), new Point(10, 10), new Point(50, 60));
            //g.DrawLine(pen, new Point(0, 0), new Point(500, 400));
            FontFamily ff = new FontFamily("times new roman");
            System.Drawing.Font font = new System.Drawing.Font(ff, 40, FontStyle.Underline);
            g.DrawString("Mahesh", font, sb, new PointF(50, 50));
          
            
        }
    }
}
