﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;

namespace Project
{
    public partial class CaptureScreen : Form
    {
        public CaptureScreen()
        {
            InitializeComponent();
            comboBox1.SelectedIndex = 0;
        }
        

        private void button1_Click(object sender, EventArgs e)
        {
            Thread t = new Thread(Blah);
            t.Start();
        }
        void Blah() 
        {
            for (; ; )
            {
                Bitmap b = new Bitmap(Screen.PrimaryScreen.WorkingArea.Width, Screen.PrimaryScreen.WorkingArea.Height);
                Graphics g = Graphics.FromImage(b);
                g.CopyFromScreen(Point.Empty, Point.Empty, Screen.PrimaryScreen.WorkingArea.Size);
                pictureBox1.Image = b;
            }
        }

        private void CaptureScreen_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Alt && e.KeyCode.ToString() == "M")
            {
                MessageBox.Show("Press M");
            }
        }

        private void textBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control && e.KeyCode.ToString() == "F")
            {
                MessageBox.Show("you press control and F");
            }
        }

        private void textBox1_Leave(object sender, EventArgs e)
        {
            if (textBox1.Text == "")
            {

                MessageBox.Show("Please fill data on textbox ");
                textBox1.Select();
            }
        }
        
        private void comboBox1_Leave(object sender, EventArgs e)
        {
            if (comboBox1.SelectedIndex == 0)
            {
                MessageBox.Show("you must select country");
                comboBox1.Select();
            }
        }

    }
}
