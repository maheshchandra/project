﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing.Drawing2D;

namespace Project
{
    public partial class Color_and_Brush : Form
    {
        public Color_and_Brush()
        {
            InitializeComponent();
        }

        private void panel1_Click(object sender, EventArgs e)
        {
            LinearGradientBrush lgb = new LinearGradientBrush(new Point(20, 20), new Point(20, 70), Color.Red, Color.Yellow);
            Graphics g = panel1.CreateGraphics();
            //g.FillRectangle(lgb, 20, 20, 50, 50);
            //g.FillEllipse(lgb, 20, 20, 150, 150);
            ColorBlend cb = new ColorBlend();
            cb.Colors = new Color[] { Color.Red, Color.Yellow, Color.Green };
            cb.Positions = new float[] { 0, .5F, 1F };
            lgb.InterpolationColors = cb;
            g.FillEllipse(lgb, 20, 20, 50, 50);

        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
