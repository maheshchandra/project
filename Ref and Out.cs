﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Project
{
    public partial class Ref_and_Out : Form
    {
        public Ref_and_Out()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int Age = 3;
            string name = "";
            modify(ref Age,out name);
            MessageBox.Show(Age.ToString());
            MessageBox.Show(name);

        }
        void modify(ref int Age,out string name)
        {
            Age += 5;
            name = "Mahesh";
        }
    }
}
