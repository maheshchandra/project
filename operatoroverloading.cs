﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Project
{
    public partial class operatoroverloading : Form
    {
        public operatoroverloading()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //Item i = (Item)3;
            Item i = 3;
            MessageBox.Show(i.Price.ToString());
            
        }
    }
    class Item
    {
        public int Price
        {
            get;
            set;
        }
        public static Item operator+(Item i1 , Item i2)
        {
        Item i3 = new Item();
        i3.Price = i1.Price + i2.Price;
        return i3;
        }
        public static bool operator ==(Item i1, Item i2)
        {
            return (i1.Price == i2.Price) ? true : false;
        }
        public static bool operator !=(Item i1, Item i2)
        {
            return (i1.Price != i2.Price) ? true : false;
        }
        public static bool operator <(Item i1, Item i2)
        {
            return (i1.Price < i2.Price) ? true : false;
        }
        public static bool operator >(Item i1, Item i2)
        {
            return (i1.Price > i2.Price) ? true : false;
        }
        public static Item operator ++(Item i)
        {
            Item i2 = new Item();
            i2.Price = i.Price + 1;
            return i2;
        }
        public static Item operator --(Item i)
        {
            Item i2 = new Item();
            i2.Price = i.Price - 1;
            return i2;
        }
        /*public static explicit operator Item(int item)
        {
            Item i = new Item();
            i.Price = item;
            return i;
        }*/
        public static implicit operator Item(int item)
        {
            Item i = new Item();
            i.Price = item;
            return i;
        }

    }
}
